

export class Router {
    constructor(data) {
        this.routes = data;
    }

    getHash () {
        let hash = window.location.hash;
        if (hash === ""){
            return "index";
        }
        hash = hash.substring(1);
        return hash;
    }

    renderComponent() {
        this.routes[this.getHash()]();
    }

    init () {
        this.renderComponent();
        window.addEventListener("hashchange", (e) => {
            if (e.oldURL != e.newURL) {
                this.routes[this.getHash()]();
            } 
        })
        // const hash = window.location.hash;
        // switch (hash) {
        //     case "":
        //         this.routes.index();
        //         break;

        //     case "#catalog":
        //         this.routes.catalog();
        //         break;

        //     case "#checkout":
        //         this.routes.checkout();
        //         break;
        
        //     default:
        //         console.log("error 404 not found, hash = " + window.location.hesh);
        //         break;
        // }
    }
}

$("#goToCatalog").click( function () {
    window.location.hash = "#catalog";
})

$("#goToCart").click( function () {
    window.location.hash = "#cart";
});

