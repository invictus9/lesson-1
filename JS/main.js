

// export function renderCategories(categories_json) {

//     const CATEGORIES_ADDRESS = "images/categories/";
//     const SUBCATEGORIES_ADDRESS = "images/categories/subcategories/";

//     for (let i = 0; i < categories_json.length; ++i) {
//         if (categories_json[i].parend_id === null) {
//             var $category = $("<div/>").addClass("menu_list");
//             const $img = $("<img>").addClass("image_icon").attr('src', CATEGORIES_ADDRESS + categories_json[i].image).attr('title', categories_json[i].title);
//             const $span = $("<span> " + categories_json[i].title + "</span>");
//             $category.append($img).append($span);
//         } else {// нашли подкатегорию
//             // console.log(categories_json[i].title)
//             let parent_title;
//             for (let j = 0; j < categories_json.length; ++j) {//найдем отца подкатегории
//                 if (categories_json[j].category_id === categories_json[i].parend_id) {
//                     parent_title = categories_json[j].title;
                
//                     break;
//                 }
//             }

//             const categories = $(".menu_list")
//             console.log(categories)
//             let category_parent = null;
//             for (let j = 0; j < categories.length; ++j) {
//                 if (categories[j].innerHTML === parent_title) {
//                     category_parent = categories[j]
//                     console.log(category_parent)
//                     break;
//                 }
//             }
//             if (!!category_parent) {// нашлась категория
//                 console.log(categories_json[i].title)
//                 // category_parent.append($("<div/>").html(categories_json[i].title))

//                 const $subCategory = $("<div/>").addClass("sub_menu_list")
//                 const $img = $("<img>").addClass("image_icon").attr('src', SUBCATEGORIES_ADDRESS + categories_json[i].image).attr('title', categories_json[i].title);
//                 const $span = $("<span> " + categories_json[i].title + "</span>");
//                 $subCategory.append($img).append($span);
//                 category_parent.append($subCategory);
//             } else {// если еще не отрисовали предка подкатегории
//                 console.log(categories_json[i].title + " !")

//             }
//         }
//         $("#menu").append($category);
//     }
// }

export function renderCategories(categories_json) {
    const CATEGORIES_ADDRESS = "images/categories/";
    const SUBCATEGORIES_ADDRESS = "images/categories/subcategories/";

    // const $

    for (let i = 0; i < categories_json.length; ++i) {
        if (categories_json[i].parend_id === null) {
            const $category = $("<div/>").addClass("menu_list");
            const $img = $("<img>").addClass("image_icon").attr('src', CATEGORIES_ADDRESS + categories_json[i].image).attr('title', 
            categories_json[i].title);
            const $span = $("<span> " + categories_json[i].title + "</span>");
            $category.append($img).append($span);
           


            for (let j = 0; j < categories_json.length; ++j) {
                if (categories_json[j].parend_id === categories_json[i].category_id) {
                    const $subCategory = $("<div/>").addClass("sub_menu_list")
                    const $img = $("<img>").addClass("image_icon").attr('src', SUBCATEGORIES_ADDRESS + categories_json[j].image).
                    attr('title', categories_json[j].title);
                    const $span = $("<span> " + categories_json[j].title + "</span>");
                    $subCategory.append($img).append($span);
                    $category.append($subCategory);
                }
            }
            $("#menu").append($category);
        }
    }
}

import _ from "lodash";

export function renderItems(items_json) {
  const pathImage = "images/items/";

  let $content = $("#content").empty();

  const  product_tpl = _.template(`
    <div class="product" id=<%=item_id%> >
      <img src="images/items/<%=img[0]%>" class="product_img">
      <div class="product_name"> <%=title%> </div>
      <div class="product_price"> <%=price%> Руб. </div>
      <div class="product_price"> <%=sizes%> </div>
    </div>
  `)
  
  for (let i = 0; i < items_json.length; ++i) { 
    $content.append(product_tpl(items_json[i]));
  }
}

// export function renderItems(items_json) {    

//     const pathImage = "images/items/"

//     $("#content").innerHTML = ""

//     for (let i = 0; i < items_json.length; i++){
//         const $block = $('<div/>').addClass("product").attr('id',items_json[i].item_id);
//         const $img = $('<img>').attr('src', pathImage + items_json[i].img[0]).addClass("product_img");
//         const $title = $('<div>' + items_json[i].title + '</div>').addClass("product_name");
//         const $price = $('<div>' + items_json[i].price + ' Руб.</div>').addClass("product_price");
//         const $size = $('<div/>').addClass("product_price").html("Размер:" + items_json[i].sizes);

//         $block.append($img).append($title).append($price).append($size);
//         $("#content").append($block)
//     }
// }


export function cleanContent() {
  $("#content").empty();
  $("#menu").empty();
}