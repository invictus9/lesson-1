
import _ from "lodash";
import * as items_json from "../data/items.json";


export class Cart {
    constructor() {
        this.addedItems = localStorage.addedItems || [];
    }

    addItemToCart(item_id) {
        if (localStorage.addedItems == undefined || localStorage.addedItems == null) {
            this.addedItems.push({
                "id" : item_id,
                "count" : 1
            });
            localStorage.addedItems = JSON.stringify(this.addedItems);
        }
        else {
            for (let i = 0; i < this.addedItems.length; ++i) {
                if (item_id == this.addedItems[i].id) {
                    this.addedItems[i].count += 1;
                    localStorage.addedItems = JSON.stringify(this.addedItems);
                    return;
                }
            }
            this.addedItems = JSON.parse(localStorage.addedItems);
            this.addedItems.push({
                "id" : item_id,
                "count" : 1
            });
            localStorage.addedItems = JSON.stringify(this.addedItems);
        }
        this.renderSmallCart();
        // this.renderInfoCartButton();
    }

    removeFromCart(item_id) {
        this.addedItems = JSON.parse(localStorage.addedItems) || [];
        let index;
        for (let i = 0; i < this.addedItems.length; ++i) {
            if (this.addedItems[i].id == item_id) {
                index = i; 
                break;
            }
        }
        this.addedItems.splice(index, 1);
        // console.log("this.addedItems after delete " + this.addedItems);
        localStorage.addedItems = JSON.stringify(this.addedItems);
        // this.renderInfoCartButton();
    }

    getItemById(id){
        for (let i = 0; i < items_json.length; ++i){
            if (id == items_json[i].item_id) {
                return items_json[i];
            }
        }
        alert("Товара нет с таким id= " + id);
    }

    renderSmallCart() {
        const $wrapperCart = $("<div/>").addClass("wrapperCart");

        const $wrapperTitle = $("<div/>").addClass("wrapperTitle").html("Корзина").appendTo($wrapperCart);
        const $buttonClose = $("<button/>").addClass("buttonRemove").appendTo($wrapperTitle).click(() => {
            $(".wrapperCart").css("display", "none");
        });
        const $buttonImg = $("<img>").attr("src", "images/close.png").appendTo($buttonClose);

        //
        this.addedItems = JSON.parse(localStorage.getItem("addedItems")) || [];
        
        const items_tmpl = `
                <% itemsCart.forEach(function(addedItem) {%> 
                    <% const item = getItemById(addedItem.id);  %>
                    <div class="itemInLightCart" id="<%=addedItem.id%>" >              
                        <button class="buttonRemoveItemInCart"></button>
                        
                        <img src="images/items/<%=item.img[0]%>" class="imgItemInCart" >
                        
                        <div class="textItemInCart" >
                            <%=item.title%> x<%=addedItem.count%>
                            <br>
                            <%=item.price*addedItem.count%> руб.
                        </div>
                    </div>
                <% }); %>
            `;
        const wrapperItem = _.template(items_tmpl);
        $wrapperCart.append(wrapperItem({
            getItemById: cart.getItemById, itemsCart: this.addedItems
        }));

        $("body").append($wrapperCart);  
        
        $(".buttonRemoveItemInCart").click(function() {
            cart.removeFromCart(this.parentNode.id);
            this.parentNode.remove();
            cart.renderSmallCart();
        })
    }

    // renderInfoCartButton () {
    //     this.addedItems = localStorage.getItem("addedItems") || [];
    //     let totalPrice;
    //     console.log();
    //     if (this.addedItems.length > 0) {
    //         totalPrice = 0;
    //         // this.addedItems.forEach(function (item) {
    //         //     totalPrice += item.price;
    //         // });
    //         for (let i = 0; i < this.addedItems.length; ++i) {
    //             console.log("item id= " + this.addedItems[i].id)
    //             totalPrice += getItemById(this.addedItems[i].id).price;
    //         }
    //         $("#goToCart").html("(" + this.addedItems.length + ") Сумма: " + totalPrice + " руб.")
    //     } else {
    //         $("#goToCart").html("В корзину");
    //     }
    // }
}


export function renderCart(items_json) {
    const  product_tpl = _.template(`
        <div class="itemInCart" >
            <img src="images/items/<%=img[0]%>" >
            <div > <%=title%> </div>
            <div ><%=price%> Руб. </div>
            <button class="">  
        </div>
    `)
    const $content = $("#content");
    const itemsCart = JSON.parse(localStorage["addedItems"]) ;
    if (!itemsCart) {
        alert("You didn`t add items to your cart.");
        return;
    } else {
        for (let i = 0; i < itemsCart.length; ++i) {
            // console.log("itemsCart.length = " + itemsCart.length);
            // console.log("items_json.length = " + items_json.length)
            for (let j = 0; j < items_json.length; ++j) {
                if (items_json[j].item_id == itemsCart[i].id) {
                    // console.log( itemsCart[i].id);
                    $content.append(product_tpl(items_json[j]));
                    break;
                }
            }
        }
    }
}


$("#goToCart").mouseenter(function () {
    $(".wrapperCart").css("display", "block");
});

// $(".wrapperCart").mouseenter( () => {
    
// });

// $(".wrapperCart").mouseleave( () => {
//     $(".wrapperCart").css("display", "none");
// });


export const cart = new Cart();
cart.renderSmallCart();