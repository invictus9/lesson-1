
// localStorage.clear();
import {cart} from "./cart.js";

const routes = {
    index: () => {
        cleanContent();
        console.log("routes-> index...")
    },
    catalog: () => {
        cleanContent();
        renderCategories(categories_json);
        renderItems(items_json);
        renderPreview(items_json);
        cart.renderSmallCart();
        // console.log("routes-> catalog...")
    },
    cart: () => {
        cleanContent();
        renderCart(items_json);
        
        // console.log("routes-> checkout...")
    },
    404: () => {
        // console.log("404 error not found");
    }
  }

import {renderCategories} from "./main.js";
import {renderItems} from "./main.js";
import {cleanContent} from "./main.js";
import {renderPreview} from "./preview.js";
import {renderCart} from "./cart.js";
import * as categories_json from "../data/categories.json";
import * as items_json from "../data/items.json";
import {Router} from "./router.js";

// renderCategories(categories_json);
// renderItems(items_json);
// renderPreview(items_json);
let router = new Router(routes);
router.init();
