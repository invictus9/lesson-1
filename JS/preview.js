import * as items_json from "../data/items.json";
import {cart} from "./cart.js";

export function renderPreview(items_json) {
    const allProducts = $(".product");
    for (let i = 0; i < allProducts.length; ++i) {
        allProducts[i].addEventListener("click", openItem.bind(null, allProducts[i].id));

    }
}

function openItem(id) {
    const ITEMS_ADDRESS = "images/items/";

    const IMAGES_ADDRESS = "images/";

    var item;
    for (let i = 0; i < items_json.length; i++) {
        if (id == items_json[i].item_id) {
            item = items_json[i];
            break;
        }
    }
    if (!item) {
        alert("There is no item on you click.");
    } else {

        const $preview = $("<div/>").addClass("preview");

        const $buttonRemovePreview = $("<button/>").addClass("buttonRemovePreview");
        const $imageButtonRemovePreview = $("<img>").attr('src', IMAGES_ADDRESS + 'close.png');
        $buttonRemovePreview.append($imageButtonRemovePreview);
        $buttonRemovePreview.click(function () {
            $preview.remove();
            $unClickBackground.remove();
        })

        //images of the product
        const $wrapperImage = $("<div/>").addClass("previewWrapperImage");
        const $imageBig = $("<img>").addClass("previewImageBig").attr('src', ITEMS_ADDRESS + item.img[0]);
        $wrapperImage.append($imageBig);

        const $wrapperImageSmall = $("<div/>").addClass("previewWrapperImageSmall");
        $wrapperImage.append($wrapperImageSmall);
        for (let i = 0; i < item.img.length; i++) {
            const $imageSmall = $("<img>").addClass("previewImageSmall").attr('src', ITEMS_ADDRESS +
                item.img[i]).click(function () {
                    $imageBig.attr('src', ITEMS_ADDRESS + item.img[i]);
                });
            $wrapperImageSmall.append($imageSmall);
        }

        //description
        const $wrapperDescription = $("<div/>").addClass("wrapperDescription");
        const $price = $("<div/>").addClass("previewDescription").html("Цена: " + item.price + " руб.");
        const $title = $("<div/>").addClass("previewDescription").html(item.title);
        const $description = $("<div/>").addClass("previewDescription").html("Описание: " + item.description);
        const $sizes = $("<div/>").addClass("previewDescription").html("Размеры: " + item.sizes);
        $wrapperDescription.append($title).append($description).append($price).append($sizes);

        //add to buy
        const $buttonAddProduct = $("<button/>").addClass("buttonAddProduct");
        const $buttonAddProductText = $("<p/>").html("Добавить в корзину");
        $buttonAddProduct.append($buttonAddProductText);
        const $imageButtonAddProduct = $("<img>").attr("src", IMAGES_ADDRESS + "add_to_cart.png").addClass("previewImageSmall");
        $buttonAddProduct.append($imageButtonAddProduct);
        $buttonAddProduct.click( () => {
            cart.addItemToCart(id);
            const $attention = $("<div/>").addClass("messageAddProduct").appendTo("body");
            $attention.html("Товар " + $title.html() + " за " + $price.html() + " руб. добавлен в корзину.");
            setTimeout( () => {
                $attention.remove();
            }, 2000);
            $preview.remove();
            $unClickBackground.remove();
        })
        $preview.append($buttonRemovePreview).append($wrapperImage).append($wrapperDescription).append($buttonAddProduct);
        
        //неактивный задний фон
        const $unClickBackground = $('<div/>').addClass("unClickBackgroundPreview").appendTo(document.body);
        $("body").append($preview);

    }

    
}

